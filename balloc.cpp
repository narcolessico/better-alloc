// printf
#include <cstdio>
// memcpy
#include <cstring>
// malloc
#include "malloc.h"
// mmap
#include <sys/mman.h>
// mutex
#include <mutex>

// Pool allocator to reduce fragmentation
#include "MemPool.hpp"

// Helpers to use libc malloc
#include "libc_functions.hpp"

// Forward all calls to libc. Disable calloc to prevent circular reference with dlsym.
#define ONLY_TRACE

// Report individual calls
//#define TRACE_ALL
// Debug information
//#define EXTRA_TRACES

// Setup logging helpers for erro, info and debug levels:
// LOGE: error, always print
// LOGT: info, always print
// LOGT: trace, print only if TRACE_ALL is enabled
// LOGD: debug, print only if EXTRA_TRACES is enabled
#define LOGI(...) fprintf(stderr, __VA_ARGS__)
#define LOGE(...) fprintf(stderr, __VA_ARGS__)
// ---
#ifdef EXTRA_TRACES
#define LOGD(...) fprintf(stderr, __VA_ARGS__)
#else
#define LOGD(...)
#endif
// ---
#ifdef TRACE_ALL
#define LOGT(...) fprintf(stderr, __VA_ARGS__)
#else
#define LOGT(...)
#endif

// Only report counters > threshold
#define REPORT_THRESHOLD (1024)

// Storage for statistics
#define TRACE_SIZES 1024
static unsigned int free_on_null = 0;
static unsigned int tot_allocations = 0;
static unsigned int peak_allocations = 0;
static size_t peak_size = 0;
static unsigned int current_allocations = 0;
static unsigned int tot_allocations_per_size[TRACE_SIZES];
static unsigned int unmatched_frees = 0;

// Storage for pre-hook mmap allocations
// No associated size -> no free; could store sizes in an additional array
#define MAX_MMAP_ALLOCS 1024
void* mmap_allocs[1024];
static unsigned int mmap_allocs_counter = 0;
static size_t mmap_tot_size = 0;

// Avoid possible recursion due to the wrapper calling dlsym calling the wrapper
static bool disable_hook = false;

static MemPool<32> pool32;
static MemPool<64> pool64;

static std::mutex alloc_free_mutex;
static std::mutex calloc_mutex;

static void stats_increment_allocs(const size_t size)
{
	current_allocations++;
	if (current_allocations > peak_allocations) peak_allocations++;
	tot_allocations++;
	if (size < TRACE_SIZES) tot_allocations_per_size[size]++;
	if (size>peak_size) peak_size = size;
}

static void stats_decrement_allocs(void* addr)
{
	if (current_allocations == 0)
	{
		LOGD("Cannot decrease already null counter. Address: %p\n", addr);
		unmatched_frees++;
	} else {
		current_allocations--;
	}
}

static void init()
{
	LOGD("Wrapper: init pointers\n");

	init_libc_functions();
}

void *mmap_alloc(size_t size)
{
	LOGT("Wrapper: mmap_alloc(%lu)\n", size);

	// anonymous -> already zeroed
	auto addr = mmap(nullptr, size, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_SHARED, -1, 0);
	if (addr == MAP_FAILED) addr = nullptr;
	mmap_allocs[mmap_allocs_counter++] = addr;
	mmap_tot_size += size;
	return addr;
}

bool is_addr_mmap(void* addr)
{
	for (int i=0; i<MAX_MMAP_ALLOCS; i++)
		if (mmap_allocs[mmap_allocs_counter]==addr) return true;
	return false;
}

void mmap_free(void* addr)
{
	LOGT("Wrapper: mmap_free(%p) -> leak\n", addr);
	// Leak: no actual munmap without length
	// munmap(addr, length?)
}

// No calloc possible if trace only
#ifndef ONLY_TRACE
void *calloc(size_t num, size_t size)
{
	LOGT("Wrapper: calloc(%lu,%lu)\n", num, size);

	std::unique_lock<std::mutex> lock(calloc_mutex);

	// No init() to avoid recursion with dlsym
	// if (!libc_calloc) init();

	if (!libc_calloc)
	{
		// Probably called from within dlsym, can't use libc_calloc; fake with mmap
		auto res = mmap_alloc(num*size);
		LOGD("  mmap->%p [%u]\n", res, mmap_allocs_counter);
		// mmap_alloc zeroes already
		return res;
	}

#if 0
	auto res = libc_calloc(num, size);
	LOGD("  libc->%p\n", res);
#else
	auto res = malloc(num*size);
	memset(res, 0, num*size);
	LOGD("  wrap->%p\n", res);
#endif
	return res;
}
#endif

void *malloc(size_t size)
{
	LOGT("Wrapper: malloc(%lu)\n", size);

	std::unique_lock<std::mutex> lock(alloc_free_mutex);

	// Update stats
	stats_increment_allocs(size);

	if (!libc_malloc) init();

#ifdef ONLY_TRACE
	return libc_malloc(size);
#endif

	void *ptr = nullptr;

	if (size==64) {
		ptr = pool64.pool_malloc();
		LOGD("  pool64->%p\n", ptr);
	} else if (size==32) {
		ptr = pool32.pool_malloc();
		LOGD("  pool32->%p\n", ptr);
	} else {
		ptr = libc_malloc(size);
		LOGD("  libc->%p\n", ptr);
	}

	//LOGD(" > malloc(%lu)->%p%s\n", size, ptr, (!ptr?" OOM?":""));
	return ptr;
}

void *realloc(void* ptr, size_t size)
{
	LOGT("Wrapper: realloc(%p,%lu)\n", ptr, size);

	std::unique_lock<std::mutex> lock(alloc_free_mutex);

	if (!libc_realloc) init();

	// Counters need to be incremented only if ptr is null,
	// otherwise everything is updated by the wrappers
	if (!ptr) stats_increment_allocs(size);

#ifdef ONLY_TRACE
	return libc_realloc(ptr,size);
#endif

	// realloc(null, s) -> malloc(s), either libc or pool
	if (!ptr) {
		//disable_hook = true;
		auto res = libc_malloc(size);
		LOGD("  libc->%p\n", res);
		//disable_hook = false;
		return res;
	}

	// Cannot realloc an address coming a mmap allocation because we did not
	// store the size. Special handling for dlsym: assume size is 32.
	if (is_addr_mmap(ptr))
	{
		if (libc_realloc)
		{
			LOGE("Warning: cannot handle realloc with mmap-allocations\n");
			return ptr;
		}
		auto newp = libc_malloc(32);
		if (!newp) return ptr;
		memcpy(newp, ptr, 32);
		mmap_free(ptr);
		auto res = libc_realloc(newp, size);
		LOGD("  rea1->%p\n", res);
		return res;
	}

	const bool from32 = pool32.isAddressInPool(ptr);
	const bool from64 = pool64.isAddressInPool(ptr);

	// If the address comes from a non-pool alloc, forward to system realloc
	if (!from32 && !from64) {
		//disable_hook = true;
		auto res = libc_realloc(ptr, size);
		//disable_hook = false;
		LOGD("  rea2->%p\n", res);
		return res;
	}

	// If address comes from a pool and size is unchanged, just return it
	if (   (from32 && size==pool32.elementSize())
		|| (from64 && size==pool64.elementSize()) ) {
		LOGD("  same->%p\n", ptr);
		return ptr;
	}

	// Address comes a from pool, size changed. System realloc should handle it, but
	// it expects the buffer size to be stored in an implementation specific way;
	// need to create a new one first, to have the size in the right place. 
	// Note in case size changes from one pool to another, pointer will be allocated
	// from libc even if the size matches the other pool.
	//disable_hook = true;
	const size_t oldSize = (from32 ? 32 : 64);
	auto newp = libc_malloc(oldSize);
	//disable_hook = false;
	if (!newp) return ptr; // Not safe but specs-compliant
	memcpy(newp, ptr, oldSize);
	if (from32) pool32.pool_free(ptr);
	if (from64) pool64.pool_free(ptr);
	//disable_hook = true;
	auto res = libc_realloc(newp, size);
	//disable_hook = false;
	LOGD("  rea3->%p\n", res);
	return res;
}

void free(void *ptr)
{
	LOGT("Wrapper: free(%p)\n", ptr);

	std::unique_lock<std::mutex> lock(alloc_free_mutex);

	if (ptr)
	{
		stats_decrement_allocs(ptr);
	} else {
		free_on_null++;
	}

	if (!libc_free) init();

#ifdef ONLY_TRACE
	return libc_free(ptr);
#endif

	if (!ptr)
	{
		LOGD("  free->nothing\n");
		return;
	} else
	if (is_addr_mmap(ptr))
	{
		LOGD("  free->mmap\n");
		mmap_free(ptr);
		return;
	} else if (pool32.isAddressInPool(ptr)) {
		pool32.pool_free(ptr);
		LOGD("  free->pool32\n");
	} else if (pool64.isAddressInPool(ptr)) {
		pool64.pool_free(ptr);
		LOGD("  free->pool64\n");
	} else {
		libc_free(ptr);
		LOGD("  free->libc\n");
	}
}

void* memalign(size_t alignment, size_t size)
{
	LOGT("Wrapper: memalign(%lu,%lu)\n", alignment, size);
	if (!libc_memalign) init();
	// Just forward
	return libc_memalign(alignment,size);
}

__attribute__ ((constructor))
void start()
{
	LOGD("Wrapper loaded\n");
	//LOGD("Wrapper: malloc: %p free: %p\n", libc_malloc, libc_free);
}

__attribute__ ((destructor))
void report()
{
	LOGI("Wrapper: peak #allocations: %u\n", peak_allocations);
	LOGI("Wrapper: peak size:         %lu (%.2gMB)\n", peak_size, peak_size/1000000.0);
	LOGI("Wrapper: free on null:      %u\n", free_on_null);
	LOGI("Wrapper: unmatched frees:   %u\n", unmatched_frees);
	LOGI("Wrapper: mmap allocations:  %u (tot %lu bytes)\n", mmap_allocs_counter, mmap_tot_size);
	LOGI("Wrapper: pool32 allocations:  %u\n", pool32.getNumAllocations());
	LOGI("Wrapper: pool64 allocations:  %u\n", pool64.getNumAllocations());
	LOGI("Wrapper: tot #allocations:  %u\n", tot_allocations);
	for (int i=0; i<TRACE_SIZES; i++)
	{
		if (tot_allocations_per_size[i]>0
			&& tot_allocations_per_size[i] > REPORT_THRESHOLD)
			LOGI("    %u bytes:\t%u\t(%0.1f%%)\n", i, tot_allocations_per_size[i],
				tot_allocations_per_size[i]*100.0/tot_allocations);
	}
}
