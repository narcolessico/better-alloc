# Balloc

Better Allocator - for test purposes.

Compiling:

	g++ balloc.cpp -shared -fPIC -o balloc.so

Using:

	LD_PRELOAD=./balloc.so PROGRAM
