#ifndef LIBC_FUNCTIONS_H
#define LIBC_FUNCTIONS_H

// dlsym
#include <dlfcn.h>

static void* (*libc_malloc)(size_t size);
static void  (*libc_free)(void *ptr);
static void * (*libc_calloc)(size_t nmemb, size_t size);
static void * (*libc_realloc)(void *ptr, size_t size);
static void * (*libc_memalign)(size_t blocksize, size_t bytes);

static void init_libc_functions()
{
	if (!libc_malloc)   libc_malloc =    (void*(*)(size_t))        dlsym(RTLD_NEXT, "malloc");
	if (!libc_free)     libc_free =      (void(*)(void*))          dlsym(RTLD_NEXT, "free");
	if (!libc_calloc)   libc_calloc =    (void*(*)(size_t,size_t)) dlsym(RTLD_NEXT, "calloc");
	if (!libc_realloc)  libc_realloc =   (void*(*)(void*,size_t))  dlsym(RTLD_NEXT, "realloc");
	if (!libc_memalign) libc_memalign =  (void*(*)(size_t,size_t)) dlsym(RTLD_NEXT, "memalign");

	if (!libc_malloc || !libc_free || !libc_calloc || !libc_realloc || !libc_memalign || !libc_calloc)
	{
		fprintf(stderr, "Wrapper: Error while retrieving functions\n");
		fprintf(stderr, "  malloc: %p free: %p calloc: %p realloc: %p memalign: %p calloc: %p\n",
			libc_malloc, libc_free, libc_calloc, libc_realloc, libc_memalign, libc_calloc);
	}
}

#endif // LIBC_FUNCTIONS_H
