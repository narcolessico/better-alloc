#include <cstdint>

#include "libc_functions.hpp"

typedef uint32_t SizeType;
typedef uint8_t Byte;

// Report individual calls
//#define TRACE_ALL

#define LOGE(...) fprintf(stderr, __VA_ARGS__)
#ifdef TRACE_ALL
#define LOGT(...) fprintf(stderr, __VA_ARGS__)
#else
#define LOGT(...)
#endif

#define POOL_SIZE (32*1024)

template<SizeType ELEMENT_SIZE>
class MemPool
{
public:
	MemPool() {
		//LOGT("MemPool: initializing with segment %p...%p\n", &m_pool[0], &m_pool[POOL_SIZE-1]);
		init_libc_functions();
	}

	~MemPool() {
		LOGT("MemPool: performed %u allocations, %u forwarded to libc implementations\n",
			m_pool_allocations, m_calls_forwarded);
	}

	void* pool_malloc()
	{
		m_pool_allocations++;

		// If the pool is full, use system malloc
		if (m_used == POOL_SIZE)
		{
			m_calls_forwarded++;
			return libc_malloc(ELEMENT_SIZE);
		}

		// All initialized ones are used; initialize next
		if (m_initialized == m_used)
		{
			m_slots[m_used] = m_used;
			m_initialized++;
		}

		auto ptr = addressFromIndex(m_slots[m_used]);
		LOGT("  pool: returning %p from slot %u\n", ptr, m_used);
		m_used++;

		return ptr;
	}

	void pool_free(void* ptr)
	{
		//init_libc_functions();
		if (!ptr)
		{
			LOGT("    pool->return\n");
			return;
		}
		// If allocation was not performed through the pool, trigger a warning
		if (!isAddressInPool(ptr))
		{
			LOGE("MemPool: attempt to free %p whcih does not belong to the pool!\n", ptr);
			return;
		}

		const auto index = indexFromAddress(ptr);
		const auto delendum = m_used - 1;

		//LOGT("    free: delendum %u, index %u, used pre %u\n", delendum, index, m_used);

		// Compact the pool: if the one being deleted is not last, swap them
		if (index < delendum)
			swapSlots(index, delendum);

		m_used--;
	}

	// Return true if in the possible range; no information about actual usage
	bool isAddressInPool(const void* address)
	{
		return (address >= (void*)(&m_pool[0])
			&& address < (void*)(&m_pool[(POOL_SIZE)*ELEMENT_SIZE]) );
	}

	SizeType elementSize()
	{
		return ELEMENT_SIZE;
	}

	SizeType getNumAllocations()
	{
		return m_pool_allocations;
	}

private:

	// Does not check that address is in the pool
	SizeType indexFromAddress(const void* address)
	{
		const auto computed_index = (SizeType)(((Byte*)address - &m_pool[0])/ELEMENT_SIZE);
		LOGT("    addr %p -> (%p-%p) -> index %u/%u\n", address,
			(Byte*)address, &m_pool[0], computed_index, POOL_SIZE);
		// Assuming address is a multiple of ELEMENT_SIZE; undefined behavior otherwise
		return computed_index;
	}

	void* addressFromIndex(const SizeType slot)
	{
		return (void*)(&m_pool[slot*ELEMENT_SIZE]);
	}

	// No range check for performance reasons. Big trust
	void swapSlots(const SizeType slot1, const SizeType slot2)
	{
		const auto tmp = m_slots[slot1];
		m_slots[slot1] = m_slots[slot2];
		m_slots[slot2] = tmp;
	}

	Byte m_pool[POOL_SIZE*ELEMENT_SIZE];
	SizeType m_slots[POOL_SIZE];
	SizeType m_used = 0;
	SizeType m_initialized = 0;

	SizeType m_pool_allocations = 0;
	SizeType m_calls_forwarded = 0;
};

#undef LOGT
#undef LOGE
#ifdef TRACE_ALL
#undef TRACE_ALL
#endif
